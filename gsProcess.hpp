//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//gsProcess.hpp, implement of Grahm Schmidt Process class

#include "gsProcess.h"

template<class T>
void gsProcess::operator()(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{
    return modified(a,v);
}

template<class T>
void gsProcess::modified(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{

    if(a.size() < 1)
        throw std::runtime_error("gsProcess requires at least 1 input");

    if(static_cast<int>(a.size()) != a[0].size())
        throw std::runtime_error("gsProcess requires dimension equal to num of inputs");

    const auto sz = a.size();
    auto i = sz;
    auto j = sz;

    gVector<T>* q = new gVector<T> [sz];

    T** r = new T* [sz];
    for(i = 0; i < sz; i++)
        r[i] = new T [sz];

    v = a;

    v[0].normal();

    for(i = 0; i < sz; i++)
    {
        if(v[i].mag() < NJ::epsilon<T>::value)
            throw std::logic_error("magnitude of vector is zero");
        r[i][i] = v[i].mag();
        q[i] = v[i] / r[i][i];
        for(j = i + 1; j < sz; j++)
        {
            r[i][j] = q[i] * v[j];
            v[j] -= r[i][j] * q[i];
        }

    }

    if(sz > 1)
    {
        for(i = 0; i < sz; i++)
            delete [] r[i];
        delete [] r;

        delete [] q;
    }
    else{
        for(i = 0; i < sz; i++)
            delete r[i];
        delete r;

        delete q;
    }


    for(auto &vec : v)
    {
        vec.normal();
    }

    return;
}

template<class T>
void gsProcess::classic(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{
    if(a.size() < 1)
        throw std::runtime_error("gsProcess requires at least 1 input");

    if(static_cast<int>(a.size()) != a[0].size())
        throw std::runtime_error("gsProcess requires dimension equal to num of inputs");


    const auto sz = a.size();
    auto i = sz;
    auto j = sz;

    gVector<T>* q = new gVector<T> [sz];

    T** r = new T* [sz];
    for(i = 0; i < sz; i++)
        r[i] = new T [sz];

    v.clear();

    for(j = 0; j < sz; j++)
    {
        v.push_back(a[j]);

        if(v[0].mag() < NJ::epsilon<T>::value)
            throw std::logic_error("magnitude of vector is zero");

        if(j == 0)
            v[0] = v[0] / v[0].mag();

        for(i = 0; i < j; i++)
        {
            r[i][j] = q[i]*a[j];
            v[j] = v[j]- r[i][j]*q[i];
        }

        if(v[j].mag() < NJ::epsilon<T>::value)
            throw std::logic_error("magnitude of vector is zero");

        r[j][j] = v[j].mag();
        q[j] = v[j] / r[j][j];
    }

    if(sz > 1)
    {
        for(i = 0; i < sz; i++)
            delete [] r[i];
        delete [] r;

        delete [] q;
    }
    else{
        for(i = 0; i < sz; i++)
            delete r[i];
        delete r;

        delete q;
    }



    for(auto &vec : v)
    {
        vec.normal();
    }

    return;
}

template<class T>
void gsProcess::price(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{
    if(a.size() < 1)
        throw std::runtime_error("gsProcess requires at least 1 input");

    if(static_cast<int>(a.size()) != a[0].size())
        throw std::runtime_error("gsProcess requires dimension equal to num of inputs");


    v = a;

    auto j = a.size();
    auto i = a.size();

    if(a[0].mag() < NJ::epsilon<T>::value)
        throw std::logic_error("magnitude of vector is zero");

    v[0] = a[0] / a[0].mag();

    for( j = 1; j < a.size(); j++)
    {
        v[j] = a[j];
        for( i = 0; i < j ; i++)
        {
            if(v[i].mag() < NJ::epsilon<T>::value)
                throw std::logic_error("magnitude of vector is zero");
            v[j] -= ((v[0] * a[j]) / (v[i].mag())) * v[i];
        }
    }
}

