# CS5201 - Object-Oriented Numerical Modeling I
## Homework 4
### Overview

You can find full details for this assignment below:

https://sites.google.com/a/mst.edu/price/courses/cs-5201/hw/2017/spring/assignment-4

### Submitting

1. Clone this repository
2. Do your homework
3. `git add` your homework to the repository
4. `git commit` your changes
5. `git push` when you are ready to submit

For more detail, see http://web.mst.edu/~nmjxv3/cs5201/git.html