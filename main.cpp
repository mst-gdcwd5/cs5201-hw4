//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//main.cpp, driver for gsProcess class

#include <iostream>
#include "driver.h"

int main(int argc, char *argv[])
{

    //threshold for which doubles will be output as zero
 
    string ifname;

    std::cout << "# Hello, World Y'All" << std::endl;

    //uses default file if not specified
    if( argc < 2)
    {
        ifname = "data/t3.txt";
    }
    else
        ifname = argv[1];

    cout << "# Opening File: " << ifname << endl;

    try
    {
        driver(ifname);
    }
    catch(const std::runtime_error& e)
    {
        cout << "# runtime error: " << e.what() << endl;
    }
    catch(const std::logic_error& e)
    {
        cout << "# logic error: "  << e.what() << endl;
    }

    
}
