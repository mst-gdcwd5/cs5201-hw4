//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//gVector.h, definition of vector class

#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>
#include "epsilon.h"

using namespace std;

//size to make vector using constructor with no paramaters
const int DEFAULT_SIZE = 3;

template<class T>
class gVector
{
private:
    T* m_data;
    int m_size = 0;

    //element access
    //pre: i >= 0, i< size of calling object
    //pst: returns reference to ith element in m_data
    T& element(const int i);

    //deallocation
    //pre:
    //pst: m_data dellocated and set to nullptr:
    //     m_size set to zero
    void clear();

    //confirm same size
    //pre:
    //pst: returns true iff calling object and vRHS have same size()
    bool samesz(const gVector<T> & vRHS) const;
public:
    //constructs empty vector with sz elements
    //pre: sz > 0
    //pst: memory allocated for sz elements
    gVector(int sz = DEFAULT_SIZE);

    //construts vector with 3 elements, set to the paramaters
    //pre: operator= defined for T
    //pst: elements set to a,b,c, respectively, size set to 3
    gVector(const T a, const T b, const T c);

    //constructs a vector by deep copy of vRHS
    //pre: operator= defined for T
    //pst: same size and deep copy of data of vRHS
    gVector(const gVector& vRHS);

    //move constructor for vRHS
    //pre: operator= defined for T
    //pst: vRHS size set to zero, data set to nullptr
    //     new object retains vRHS m_data pointer
    //     size matches vRHS
    gVector(gVector<T>&& vRHS);

    //explicit move
    //pre:
    //pst: calling object updated with size of vRHS
    //     calling object gets vRHS data pointer
    //     vRHS set to size zero, and pointer to null
    void move(gVector<T>&& vRHS);

    //destructor
    //pre:
    //pst: dynamic data deallocated
    ~gVector();

    //copy assignment
    //pre:
    //pst: deep copy of data of vRHS
    //     size of calling object updated to size of vRHS
    gVector<T> & operator=(const gVector<T> & vRHS);

    //element access
    //pre: i >= 0, i< size of calling object
    //pst: returns reference to ith element in m_data
    T & operator[](const int i) {return element(i);}

    //element access
    //pre: i >= 0, i< size of calling object
    //pst: returns reference to ith element in m_data
    T get(const int i) const;

    //current m_data allocated size
    //pre:
    //pst: returns allocated size of m_data
    int size() const {return m_size;}

    //deletes and reallocates m_data
    //pre: i >= 0
    //pst: data cleared, reallocated to input size
    //     m_size updated to new size
    void resize(const int i = DEFAULT_SIZE); //private?

    //adds elements of vRHS to respective elements of calling object
    //pre: operator+= defined for T
    //     calling object & vRHS have same size
    //pst: calling object updated with addition of vRHS
    gVector<T> & operator+=(const gVector<T> & vRHS);

    //subtracts elements of vRHS to respective elements of calling object
    //pre: operator+= and operator-(negation) defined for T
    //     calling object & vRHS have same size
    //pst: calling object updated with subtraction of vRHS
    gVector<T> & operator-=(const gVector<T> & vRHS);

    //returns object that is negation of calling object
    //pre: operator-(negation) defined for T
    //pst: returns seperate object, negation of calling object
    gVector<T> operator-() const;

    //returns object, result of addition of calling and vRHS
    //pre: operator+= defined for T
    //     calling object & vRHS have same size
    //pst: new object returned with resutl of addition
    gVector<T> operator+(const gVector<T> & vRHS) const;

    //returns object, result of subtraction of vRHS from calling
    //pre: operator+= and operator-(negation) defined for T
    //     calling object & vRHS have same size
    //pst: new object returned with result of addition
    gVector<T> operator-(const gVector<T> & vRHS) const;

    //returns object, result of dot product or calling and vRHS
    //pre: operator*(multiplication) defined for T
    //     calling object & vRHS have same size
    //pst: new object returned with result of dot product
    T operator*(const gVector<T> & vRHS) const;

    //compares elements of calling and vRHS
    //pre: operator== defined for T
    //     calling object & vRHS have same size
    //pst: returns true iff all respective elements ==
    bool operator==(const gVector<T> & vRHS) const;

    //negation of operator==
    //pre: operator== defined for T
    //     calling object & vRHS have same size
    //pst: returns true iff !operator==
    bool operator!=(const gVector<T> & vRHS) const;

    //magnitude of vector
    //pre: operator* defined for T
    //pst: returns magnitude of vector
    T mag() const {return sqrt(*this * *this);}

    //normalize vector
    //pre: 
    //pst: if nonzero, normalized, otherwise, nothing
    gVector<T> & normal();

};


//scalar multiplication
//each element of vRHS operator* with i
//pre: operator*(multiplication) defined for T and R
//pst: new object returned with result of scalar multiplication
template<class T, class R>
gVector<R> operator*(const T i, const gVector<R> & vRHS);

//scalar division
//each element of vRHS operator/ with i
//pre: operator/(division) defined for T and R
//pst: new object returned with result of scalar division
template<class T, class R>
gVector<T> operator/(const gVector<T> & vLHS, const R r);

//~~pretty print~~
//pre:
//pst: outputs operator<< for each element in sequence
//     followed by single space
//     prints nothing if size() == 0
template<class T>
ostream& operator<<(ostream& os, const gVector<T> & vRHS);

//input into vector
//input checking provided only for float and int types for T
//pre: size() set to desired number of elements
//pst: size() elements read in and vRHS upated
template<class T>
istream& operator>>(istream& is, const gVector<T> & vRHS);

//input from file
//pre:  fname references file with format...
//      first line of file is size of vectors AND
//          number to be read in
//      following, one vector per line, elements seperated with
//          whitespace
//pst:  vector data contains the set of vectors represented in file
template<typename T>
void fileInput(const string& fname, vector<gVector<T>> &data);


//input from stream
//pre:  stream references data with format...
//      first line of data is size of vectors AND
//          number to be read in
//      following, one vector per line, elements seperated with
//          whitespace
//pst:  vector data contains the set of vectors represented in stream
template<typename T>
void fileInput(istream &fi, vector<gVector<T>> & data);

#include "gVector.hpp"