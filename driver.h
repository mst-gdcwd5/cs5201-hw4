//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//main.cpp, driver for gsProcess class

#pragma once

#include "epsilon.h"
#include "gsProcess.h"
#include "gVector.h"
#include "epsilon.h"

//DSC: Driver for GS Process
//PRE: ifname is name of file in vector format
//      first line - integer dimension/number of vectors
//      line for each vector, elements seperated with ws
//      has at least one vector
//PST: input, modified, magnitudes, and dot products
//      output to cout
void driver(string ifname)
{
    
    using T = double; //float precision for driver

    T result;
    
    gsProcess func;
    vector<gVector<T>> idata;
    vector<gVector<T>> rdata; 

    fileInput(ifname, idata);

    std::cout.precision(NJ::epsilon<T>::precision);

    //output data read in
    cout << "# input" << endl;
    for(auto const &vec : idata)
    {
        cout << vec << endl;
    }

    func(idata, rdata);

    //output results of MIT-Modified Gram-Schmidt algorithm
    cout << "# modified" << endl;
    for(auto const &vec : rdata)
    {
        cout << vec << endl;
    }

    cout << "# magnitudes" << endl;
    auto k = rdata.size();
    for(k = 0; k < rdata.size(); k++)
    {
        cout << "# " << k << ", " << rdata[k].mag() << endl;
    }

    //outputs dot products of results
    auto i = rdata.size();
    auto j = rdata.size();
    cout << "# dot products" << endl;
    for( i = 0; i < rdata.size(); i++)
    {
        for( j = 0; j < rdata.size(); j++)
        {
            if(i != j)
            {
                result = rdata[i] * rdata[j];

                cout << "# (" << i << "," << j << ") ";
                cout << (abs(result) < NJ::epsilon<T>::value ? "true" : "false") << endl;
                cout << (abs(result) < NJ::epsilon<T>::value ? 0 : result) << endl;
            }
        }
    }

    
}