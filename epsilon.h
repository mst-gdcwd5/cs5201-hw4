#pragma once

namespace NJ{

template<class T>
struct epsilon
{
  constexpr static T value = 0;
  constexpr static float precision = 6;
};

template<>
struct epsilon<double>
{
  constexpr static double value = 1e-8;
  constexpr static float precision = 8;
};

template<>
struct epsilon<float>
{
  constexpr static float value = 1e-6f;
  constexpr static float precision = 6;
};

}


