#define BOOST_TEST_MODULE gVector_test
#include <boost/test/included/unit_test.hpp>
#include "gVector.h"
#include "gsProcess.h"
#include <sstream>
#include "epsilon.h"
#include <fstream>
#include "driver.h"

using namespace std;

const double SMALL_THRESHOLD = NJ::epsilon<double>::value;

BOOST_AUTO_TEST_SUITE( qv_internal )

BOOST_AUTO_TEST_CASE( gv_construct )
{
	gVector<int> t1;

	for(int i = 0; i < DEFAULT_SIZE; i++)
	{
		t1[i] = i;
		BOOST_CHECK_EQUAL(t1[i], i);
	}

    BOOST_CHECK_EQUAL(t1.size(), DEFAULT_SIZE);

	const int t2_sz = 100;
	gVector<int> t2(t2_sz);
    BOOST_CHECK_EQUAL(t2.size(), t2_sz);

	for(int i = 0; i < t2_sz; i++)
	{
		t2[i] = i;
		BOOST_CHECK_EQUAL(t2.get(i), i);
	}

	gVector<int> t3(t2);
	t2.resize(); //ensure deep copy?
    BOOST_CHECK_EQUAL(t2.size(), DEFAULT_SIZE);
    BOOST_CHECK_EQUAL(t3.size(), t2_sz);

	for(int i = 0; i < t2_sz; i++)
	{
		BOOST_CHECK_EQUAL(t3[i], i);
	}

	// can move consructor be tested?
	
	//test sizing?
}

BOOST_AUTO_TEST_CASE( gv_assign )
{
	gVector<int> t1(1,2,3);
	gVector<int> t2;
	gVector<int> t3;

	t2 = t1;
	t3 = t1;

	t1 += t2;

	for(int i = 0; i < 3; i++)
	{
		BOOST_CHECK_EQUAL(t1[i], (i+1)*2);
	}

	t3 -= t2;
	t1.resize(2);

	for(int i = 0; i < 3; i++)
	{
		BOOST_CHECK_EQUAL(t3[i], 0);
	}

}

BOOST_AUTO_TEST_CASE( gv_math )
{
    gVector<int> t1(1,2,3);
    gVector<int> t2(1,2,3);
    gVector<int> t3, t4, t5;

    t3 = t1 + t2;
    t4 = t1 - t2;
    t5 = -t3;


    for(int i = 0; i < 3; i++)
    {
        BOOST_CHECK_EQUAL(t3[i], (i+1)*2);
        BOOST_CHECK_EQUAL(t4[i], 0);
        BOOST_CHECK_EQUAL(t5[i], (i+1)*-2);
    }

}

BOOST_AUTO_TEST_CASE( gv_scalar_math )
{
    gVector<int> t1(1,2,3);
    gVector<int> t2;
    t2 = 5 * t1;

    for(int i = 0; i < 3; i++)
    {
        BOOST_CHECK_EQUAL(t2[i], (i+1)*5);
    }

    gVector<float> t3(5,10,15);
    gVector<float> t4;
    t4 = t3 / 5;

    for(int i = 0; i < 3; i++)
    {
        BOOST_CHECK_EQUAL(t4[i], (i+1));
    }

}

BOOST_AUTO_TEST_CASE( gv_product )
{
    gVector<int> t1(1,2,3);
    gVector<int> t2(3,4,5);

    BOOST_CHECK_EQUAL(t1 * t2, 26);
}

BOOST_AUTO_TEST_CASE( gv_compare )
{
    gVector<int> t1(1,2,3);
    gVector<int> t2(t1);
    gVector<int> t3(4,5,6);

    BOOST_CHECK_EQUAL(t1 == t2, true);
    BOOST_CHECK_EQUAL(t1 == t3, false);
}

BOOST_AUTO_TEST_CASE( gv_mag )
{
    gVector<double> t1(3,4,5);

    BOOST_CHECK_CLOSE(t1.mag(),7.07107, 0.01);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( qv_friends )

BOOST_AUTO_TEST_CASE( gv_input )
{
    stringstream si("1 2 3 ");
    gVector<int> t1;
    gVector<int> t2(1,2,3);

    si >> t1;

    BOOST_CHECK_EQUAL(t1, t2);

}

BOOST_AUTO_TEST_CASE( gv_output )
{
    stringstream so;
    string sr("1 2 3 ");
    gVector<int> t1(1,2,3);

    so << t1;

    string st = so.str();

    BOOST_REQUIRE_EQUAL(sr.length(), st.length());

    auto i = sr.length();
    for(i = 0; i < sr.length(); i++)
        BOOST_CHECK_EQUAL(sr[i], st[i] );

}

BOOST_AUTO_TEST_CASE( gv_file )
{
     vector<gVector<int>> tdata;
     vector<int> rdata;
     vector<int> sdata;
     int sum = 0;

     rdata.push_back(5);
     rdata.push_back(3);
     rdata.push_back(5);
     rdata.push_back(3);
     rdata.push_back(4);

     fileInput("data/t1.txt", tdata);

     for(auto const &gv : tdata)
     {
        sum = 0;
        for(int i = 0; i < gv.size(); i++)
            sum += gv.get(i);
        sdata.push_back(sum);
     }

     BOOST_REQUIRE_EQUAL(rdata.size(), sdata.size());

     auto i = rdata.size();
     for(i = 0; i < rdata.size(); i++)
        BOOST_CHECK_EQUAL(rdata[i], sdata[i]);


}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( qs )

BOOST_AUTO_TEST_CASE( gs_modified )
{
    gsProcess func;
    vector<gVector<double>> idata;
    vector<gVector<double>> rdata;

    fileInput("data/t1.txt", idata);

    func(idata, rdata);

    auto i = rdata.size();
    auto j = rdata.size();
    for( i = 0; i < rdata.size(); i++)
    {
        for( j = i+1; j < rdata.size(); j++)
        {
            if(i != j)
            {
                BOOST_CHECK_SMALL(rdata[i] * rdata[j], SMALL_THRESHOLD);
            }  
        }
    }
}

BOOST_AUTO_TEST_CASE( gs_classic )
{
    gsProcess func;
    vector<gVector<double>> idata;
    vector<gVector<double>> rdata;

    fileInput("data/t1.txt", idata);

    func.classic(idata, rdata);

    auto i = rdata.size();
    auto j = rdata.size();
    for( i = 0; i < rdata.size(); i++)
    {
        for( j = i+1; j < rdata.size(); j++)
        {
            if(i != j)
            {
                BOOST_CHECK_SMALL(rdata[i] * rdata[j], SMALL_THRESHOLD);
            }  
        }
    }

}

BOOST_AUTO_TEST_CASE( gs_price )
{
    gsProcess func;
    vector<gVector<double>> idata;
    vector<gVector<double>> rdata;

    fileInput("data/t1.txt", idata);

    func.price(idata, rdata);

    auto i = rdata.size();
    auto j = rdata.size();
    for( i = 0; i < rdata.size(); i++)
    {
        for( j = i+1; j < rdata.size(); j++)
        {
            if(i != j)
            {
                BOOST_WARN_SMALL(rdata[i] * rdata[j], SMALL_THRESHOLD);
            }    
        }
    }

}


BOOST_AUTO_TEST_CASE( gs_large )
{
    gsProcess func;
    vector<gVector<double>> idata;
    vector<gVector<double>> rdata;

    fileInput("data/t3.txt", idata);

    func(idata, rdata);

    BOOST_REQUIRE_EQUAL(idata.size(), rdata.size());

    for(auto vec : rdata)
    {
        BOOST_CHECK_SMALL(vec.mag()-1, SMALL_THRESHOLD);
    }

    auto i = rdata.size();
    auto j = rdata.size();
    for( i = 0; i < rdata.size(); i++)
    {
        for( j = i+1; j < rdata.size(); j++)
        {
            if(i != j)
            {
                BOOST_CHECK_SMALL(rdata[i] * rdata[j], SMALL_THRESHOLD);
            }
        }
    }

}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE( gs_integrate )

BOOST_AUTO_TEST_CASE( gs_random )
{

    gsProcess func;
    vector<gVector<double>> idata;
    vector<gVector<double>> rdata;

    string ofname = "s1.txt";
    ofstream os;
    os.open(ofname);

    srand(static_cast<unsigned int>(time(NULL)));

    int tn = rand() % 20;
    
    os << tn << endl;
    for(int i = 0; i < tn; i++)
    {
        for(int j = 0; j < tn; j++)
        {
            os << static_cast<float>(rand()) / static_cast<float>(rand()) << " ";
        }
        os << endl;
    }

    os.close();

    fileInput(ofname, idata);

    func(idata, rdata);

    BOOST_REQUIRE_EQUAL(idata.size(), rdata.size());

    for(auto vec : rdata)
    {
        BOOST_CHECK_SMALL(vec.mag()-1, SMALL_THRESHOLD);
    }

    auto i = rdata.size();
    auto j = rdata.size();
    for( i = 0; i < rdata.size(); i++)
    {
        for( j = i+1; j < rdata.size(); j++)
        {
            if(i != j)
            {
                BOOST_CHECK_SMALL(rdata[i] * rdata[j], SMALL_THRESHOLD);
            }
        }
    }

}

BOOST_AUTO_TEST_SUITE_END()




